<?php

echo "Use of addslashes function!<br>";

$string1= "I am a BITM 'student";

echo addslashes($string1);

echo "<br> Use of explode function!<br>";

$string2="Hello friends!It's a beatiful day.";

echo (explode(" ",$string2));

echo "<br> Use of implode function<br>";

$array1=array('This','a','single','array');

echo join(" ",$array1);

echo "<br>Use of trim function<br>";

$string3="Hello friends!";
echo "Without Trim: ".$string3;
echo "<br>";
echo "With Trim: ".trim($string3,"He");

echo "<br> Use of Money_format function!<br>";

$number1=1234.56;
setlocale(LC_MONETARY,"en_US");
//echo money_format("The price is %i",$number1);

echo "<br>Use of nl2br function<br>";

echo nl2br("One line.\nAnother line.");

echo "<br>Use of str_repeat function<br>";

echo str_repeat(".",2);

echo "<br>Use of str_replace function<br>";

$array2=array("blue","green","yellow");
print_r(str_replace("yellow","red",$array2,$i));
echo "<br>". "Replacement: $i";

echo "<br>Use of str_split function<br>";

print_r(str_split("Hello",3));

echo "<br>Use of strip_tags function<br>";

print_r(strip_tags("I am a <b><i> BITM student </i></b>","<b>"));

echo "<br>Use of strlen function<br>";

echo strlen("Sumona");

echo "<br>Use of strtolower function<br>";

echo strtolower("I AM A BITM STUDENT");

echo "<br>Use of strtoupper function<br>";

echo strtoupper("I am a bitm student");

echo "<br>Use of substr_compare function<br>";

echo substr_compare("Hello BITM","WORLD",6);

echo "<br>Use of substr_count function<br>";

$string4="This is a test";
echo(substr_count($string4,"is",3,3));

echo "<br>Use of substr_replace function<br>";

echo substr_replace("BITM","Hello",0,0);

echo "<br>Use of ucfirst function<br>";

echo ucfirst("hello world!");
echo "<br> Use of htmlentites function <br>";
 $string5="<@w3schools>";
echo htmlentities($string5);

echo "<br> Use of str_pad function <br>";

$string6="Hello World!";
echo str_pad($string6,20,".");

?>
